package com.tm.newsparsdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewsparsdbApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewsparsdbApplication.class, args);
	}

}
