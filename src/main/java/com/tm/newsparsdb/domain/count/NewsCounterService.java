package com.tm.newsparsdb.domain.count;

public interface NewsCounterService {
    public int count(final String str);
}
