package com.tm.newsparsdb.domain.count;

import com.tm.newsparsdb.services.dao.NewsOpennet;
import com.tm.newsparsdb.services.service.OpennetService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class OpenParsHead implements ParsHead{
    private final String searchPatternHead;
    @Autowired
    private OpennetService opennetService;

    public OpenParsHead(final String searchPattern) {
        this.searchPatternHead = searchPattern;
    }

    @Override
    public void returnHeadList(final String page, final int count) {

        final Pattern pattern = Pattern.compile(searchPatternHead);
        final Matcher matcher = pattern.matcher(page);
        final List<String> list = new LinkedList<>();
        while (matcher.find()) {
            list.add(matcher.group(1));
        }

        List<String> listResult = IntStream.range(0, count)
                .mapToObj(list::get)
                .collect(Collectors.toCollection(LinkedList::new));

        listResult.forEach(s -> {
            NewsOpennet newsOpennet = new NewsOpennet();
            newsOpennet.setHead(s);
            opennetService.save(newsOpennet);
        });
    }
}
