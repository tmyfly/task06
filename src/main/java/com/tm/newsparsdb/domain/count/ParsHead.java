package com.tm.newsparsdb.domain.count;

import java.util.List;

public interface ParsHead {
    public void returnHeadList(final String page, final int count);
}
