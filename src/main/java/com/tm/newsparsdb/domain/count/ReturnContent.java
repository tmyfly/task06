package com.tm.newsparsdb.domain.count;

public interface ReturnContent {
    public String contentReturn(final String url);
}
