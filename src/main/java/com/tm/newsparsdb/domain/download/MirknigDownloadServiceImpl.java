package com.tm.newsparsdb.domain.download;

import com.tm.newsparsdb.domain.count.ReturnContent;
import org.springframework.beans.factory.annotation.Qualifier;

public class MirknigDownloadServiceImpl extends AbstractDownloadService{
    public MirknigDownloadServiceImpl(final String url, @Qualifier("rc2") final ReturnContent returnContent) {
        super(url, returnContent);
    }
}
