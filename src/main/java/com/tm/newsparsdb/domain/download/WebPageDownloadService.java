package com.tm.newsparsdb.domain.download;

public interface WebPageDownloadService {
    public String download();
}
