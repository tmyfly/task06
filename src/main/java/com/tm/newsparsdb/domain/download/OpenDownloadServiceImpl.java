package com.tm.newsparsdb.domain.download;

import com.tm.newsparsdb.domain.count.ReturnContent;
import org.springframework.beans.factory.annotation.Qualifier;

public class OpenDownloadServiceImpl extends AbstractDownloadService{

    public OpenDownloadServiceImpl(final String url, @Qualifier("rc1") final ReturnContent returnContent) {
        super(url, returnContent);
    }
}
