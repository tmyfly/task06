package com.tm.newsparsdb.services.dto;

public class NewsOpennet {
    private Long id;
    private String head;

    public NewsOpennet(Long id, String head) {
        this.id = id;
        this.head = head;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    @Override
    public String toString() {
        return "NewsOpennet{" +
                "id=" + id +
                ", head='" + head + '\'' +
                '}';
    }
}
