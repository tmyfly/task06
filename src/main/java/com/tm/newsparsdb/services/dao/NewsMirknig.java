package com.tm.newsparsdb.services.dao;

import javax.persistence.*;

@Entity
@Table(name = "mirnewshead")
public class NewsMirknig {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "head")
    private String head;

    public NewsMirknig() {
    }

    public NewsMirknig(Long id, String head) {
        this.id = id;
        this.head = head;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    @Override
    public String toString() {
        return "NewsMirknig{" +
                "id=" + id +
                ", head='" + head + '\'' +
                '}';
    }
}
