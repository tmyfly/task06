package com.tm.newsparsdb.services.controller;

import com.tm.newsparsdb.app.NewsCounterUcsOpenImpl;
import com.tm.newsparsdb.services.dao.NewsOpennet;
import com.tm.newsparsdb.services.service.OpennetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/opennet")

public class OpennetController {
    @Autowired
    private NewsCounterUcsOpenImpl newsCounterUcs;

    @Autowired
    private OpennetService opennetService;

    @GetMapping
    public String opennetPage(Model model){
        List<NewsOpennet> newsOpennets = opennetService.getAllHead();
        if (newsOpennets.size() == 0){
            newsCounterUcs.run();
            newsOpennets = opennetService.getAllHead();
        }

        model.addAttribute("listHead", newsOpennets);
        return "opennet";
    }
}
