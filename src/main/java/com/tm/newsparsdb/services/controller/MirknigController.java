package com.tm.newsparsdb.services.controller;

import com.tm.newsparsdb.app.NewsCounterUcsMirknigImpl;
import com.tm.newsparsdb.services.dao.NewsMirknig;
import com.tm.newsparsdb.services.service.MirknigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/mirknig")
public class MirknigController {
    @Autowired
    private NewsCounterUcsMirknigImpl newsCounterUcsMirknig;
    @Autowired
    private MirknigService mirknigService;

    @GetMapping
    public String mirknigPage(Model model){
        List<NewsMirknig> newsMirknig = mirknigService.getAllHead();
        if (newsMirknig.size() == 0){
            newsCounterUcsMirknig.run();
            newsMirknig = mirknigService.getAllHead();
        }

        model.addAttribute("listHead", newsMirknig);
        return "mirknig";
    }
}
