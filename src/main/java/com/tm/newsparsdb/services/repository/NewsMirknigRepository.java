package com.tm.newsparsdb.services.repository;

import com.tm.newsparsdb.services.dao.NewsMirknig;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsMirknigRepository extends CrudRepository<NewsMirknig, Long> {

}
