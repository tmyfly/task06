package com.tm.newsparsdb.services.repository;

import com.tm.newsparsdb.services.dao.NewsOpennet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewsOpennetRepository extends CrudRepository<NewsOpennet, Long> {

}
