package com.tm.newsparsdb.services.service;

import com.tm.newsparsdb.services.dao.NewsMirknig;
import com.tm.newsparsdb.services.repository.NewsMirknigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MirknigService {

    @Autowired
    private NewsMirknigRepository newsMirknigRepository;

    public void save(NewsMirknig newsMirknig){
        newsMirknigRepository.save(newsMirknig);
    }

    public List<NewsMirknig> getAllHead(){
        return (List<NewsMirknig>) newsMirknigRepository.findAll();
    }
}
