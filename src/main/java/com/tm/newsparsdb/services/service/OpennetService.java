package com.tm.newsparsdb.services.service;

import com.tm.newsparsdb.services.dao.NewsOpennet;
import com.tm.newsparsdb.services.repository.NewsOpennetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OpennetService {

    @Autowired
    private NewsOpennetRepository newsOpennetRepository;

    public void save(NewsOpennet newsOpennet){
        newsOpennetRepository.save(newsOpennet);
    }

    public List<NewsOpennet> getAllHead(){
        return (List<NewsOpennet>) newsOpennetRepository.findAll();
    }
}
