package com.tm.newsparsdb.app;

import com.tm.newsparsdb.domain.count.NewsCounterService;
import com.tm.newsparsdb.domain.count.ParsHead;
import com.tm.newsparsdb.domain.download.WebPageDownloadService;
import com.tm.newsparsdb.domain.printing.NewsPrintingService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class NewsCounterUcsMirknigImpl implements NewsCounterUcs {

    private final WebPageDownloadService webPageDownloadService;
    private final NewsCounterService newsCounterService;
    private final NewsPrintingService newsPrintingService;
    private final ParsHead mirknigParsHead;

    public NewsCounterUcsMirknigImpl(@Qualifier("wpd2") final WebPageDownloadService webPageDownloadService,
                                     @Qualifier("ncs2") final NewsCounterService newsCounterService,
                                     @Qualifier("nps2") final NewsPrintingService newsPrintingService,
                                     @Qualifier("ph2") final ParsHead mirknigParsHead) {
        this.webPageDownloadService = webPageDownloadService;
        this.newsCounterService = newsCounterService;
        this.newsPrintingService = newsPrintingService;
        this.mirknigParsHead = mirknigParsHead;
    }

    public void run(){
        final String page = webPageDownloadService.download();
        final int result = newsCounterService.count(page);
        mirknigParsHead.returnHeadList(page,result);
    }
}
